//Powered By if, Since 2014 - 2020

package com.zs.pig.cms.mapper;

import com.github.abel533.mapper.Mapper;
import com.zs.pig.cms.api.model.CmsSite;


/**
 * 
 * @author zs 2016-5-24 21:51:02
 * @Email: 951449465@qq.com
 * @version 4.0v
 *	我的cms
 */
public interface CmsSiteMapper extends Mapper<CmsSite>{

}
