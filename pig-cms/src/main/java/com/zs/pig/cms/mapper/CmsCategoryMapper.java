//Powered By if, Since 2014 - 2020

package com.zs.pig.cms.mapper;

import com.github.abel533.mapper.Mapper;
import com.zs.pig.cms.api.model.CmsCategory;


/**
 * 
 * @author zs 2016-5-24 21:52:07
 * @Email: 951449465@qq.com
 * @version 4.0v
 *	我的cms
 */
public interface CmsCategoryMapper extends Mapper<CmsCategory>{

}
