//Powered By zsCat, Since 2016 - 2020

package com.zs.pig.[model].api.service;

import com.zs.pig.[model].api.model.CmsArticle;
import com.zs.pig.common.base.BaseService;

/**
* @author [author] [date]
 * @Email: 951449465@qq.com
 * @version 4.0v
 *	[description]
 */

public interface [entityClass]Service extends BaseService<[entityClass]>{

	

	
	/**
	 * 保存或更新
	 * 
	 * @param [entityClass]
	 * @return
	 */
	public int save[entityClass]([entityClass] record) ;
	/**
	 * 删除
	* @param CmsArticle
	* @return
	 */
	public int delete[entityClass]([entityClass] record);


}
